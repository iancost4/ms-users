import { NotFoundException } from '@nestjs/common';

export const exceptionsMessages = {
  internalServerError: 'Internal Server Error',
};

export const notFound = (field: string) => {
  throw new NotFoundException(`${field} not found`);
};
