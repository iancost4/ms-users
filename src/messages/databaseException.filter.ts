import { FastifyReply } from 'fastify';
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { BaseError, ValidationError } from 'sequelize';

@Catch(ValidationError, BaseError)
export class DatabaseExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<FastifyReply>();
    let message: string;

    if (exception instanceof ValidationError) {
      message = exception.errors[0].message;
    } else {
      message = exception.message;
    }

    response.code(400).send({ message });
  }
}
