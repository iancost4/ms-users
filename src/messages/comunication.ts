export const comunicationMessages = {
  default: {
    invalid: (field: string) => `Campo ${field} inválido`,
    required: (field: string) => `O campo ${field} é obrigatorio`,
    uppercase: (field: string) => `O campo ${field} precisa ser maiúsculo`,
    number: (field: string) => `O campo ${field} precisa ser do tipo número`,
    string: (field: string) => `O campo ${field} precisa ser do tipo texto`,
    boolean: (field: string) => `O campo ${field} precisa ser do tipo boleando`,
    stringNumber: (field: string) =>
      `O campo ${field} precisa ser do tipo texto contendo números`,
    minLength: (field: string, length: number) =>
      `O campo ${field} precisa ter no mínimo ${length} caracteres`,
    maxLength: (field: string, length: number) =>
      `O campo ${field} precisa ter no máximo ${length} caracteres`,
  },
};
