import {
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { notFound } from '@/messages/exceptions';
import * as bcrypt from 'bcrypt';

import User from '@/modules/v1/users/entities/user.entity';
import UserDto from './dto/user.dto';
import { UserCreateDto } from '@/modules/v1/users/dto/userCreate.dto';
import { UserUpdateDto } from './dto/userUpdate.dto';

import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User)
    private userRepository: typeof User,
    private jwtTokenService?: JwtService,
  ) {}

  /**
   * Find User
   *
   * @param {number} id
   *
   * @returns {User}
   */
  async show(id: number): Promise<User> {
    const user = await this.userRepository.findByPk(id);

    if (!user) notFound('User');

    user.password = undefined;
    user.salt = undefined;

    return user;
  }

  /**
   * Find User
   *
   * @param {string} email
   *
   * @returns {User}
   */
   async showByEmail(email: string): Promise<UserDto> {
    const user = await this.userRepository.findOne({
      where: {
        email,
      },
    });

    if (!user) notFound('User');

    return user;
  }

  /**
   * Create User
   *
   * @param {UserCreateDto} UserCreateDto
   *
   * @returns {User}
   */
  async store(userCreateDto: UserCreateDto): Promise<User> {
    userCreateDto.salt = await bcrypt.genSalt();
    userCreateDto.password = await this.hashPassword(userCreateDto.password, userCreateDto.salt);

    const user = await this.userRepository.create(userCreateDto);

    user.password = undefined;
    user.salt = undefined;

    return user;
  }

  /**
   * Create User
   *
   * @param {UserUpdateDto} userUpdateDto
   *
   * @returns {User}
   */
   async update(id: number, userUpdateDto: UserUpdateDto): Promise<User> {
    const user = await this.userRepository.findByPk(id);

    if (userUpdateDto.password) {
      userUpdateDto.password = await this.hashPassword(userUpdateDto.password, user.salt);
    };

    await user.update(userUpdateDto);

    user.password = undefined;
    user.salt = undefined;
  
    return user;
  }

  async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  async loginWithCredentials(userDto: UserDto): Promise<Partial<UserDto>> {
    const payload = { 
      email: userDto.email,
      sub: userDto.id,
      name: userDto.name,
      gender: userDto.gender,
    };

    return {
      id: userDto.id,
      name: userDto.name,
      email: userDto.email,
      cellphone: userDto.cellphone,
      gender: userDto.gender,
      professionId: userDto.professionId,
      lastAccess: userDto.lastAccess,
      accessToken: this.jwtTokenService.sign(payload),
    };
  }
}
