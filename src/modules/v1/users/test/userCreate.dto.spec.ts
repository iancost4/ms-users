import { validateSpecificDto } from '@/validations/validateSpecificDto.validator';
import { UserCreateDto } from '@/modules/v1/users/dto/userCreate.dto';

describe('Test User Create DTO', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Validation required fields', async () => {
    const validation: Array<any> = await validateSpecificDto(UserCreateDto, {});

    expect(5).toEqual(validation.length);
  });
});
