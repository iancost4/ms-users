import * as faker from 'faker';

import { mockUser } from '@/mocks/user.mock';
import { validateSpecificDto } from '@/validations/validateSpecificDto.validator';
import UserDto from '@/modules/v1/users/dto/user.dto';
import commonValidations from '@/utils/common-validations';

describe('Test User DTO', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Validation required fields', async () => {
    const validation: Array<any> = await validateSpecificDto(UserDto, {});
    expect(7).toEqual(validation.length);

    commonValidations(validation, 'name');
    commonValidations(validation, 'email');
    commonValidations(validation, 'password');
    commonValidations(validation, 'salt');
    commonValidations(validation, 'gender');
    commonValidations(validation, 'professionId');
    commonValidations(validation, 'lastAccess');
  });

  it('Validate if is the correct type', async () => {
    const successCase: Array<any> = await validateSpecificDto(
      UserDto,
      mockUser,
    );

    expect(0).toEqual(successCase.length);

    const failedCase: Array<any> = await validateSpecificDto(UserDto, {
      id: faker.random.word(),
      name: faker.datatype.number(),
      email: faker.datatype.number(),
      password: faker.datatype.number(),
      salt: faker.datatype.number(),
      cellphone: faker.datatype.number(),
      cpf: faker.datatype.number(),
      gender: faker.datatype.number(),
      professionId: faker.random.word(),
      lastAccess: faker.datatype.number(),
      accessToken: faker.datatype.number(),
    });

    expect(11).toEqual(failedCase.length);
  });
});
