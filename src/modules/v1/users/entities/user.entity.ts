import { Table, Model, Column, DataType, CreatedAt, UpdatedAt, DeletedAt } from 'sequelize-typescript';
import { GenderEnum, UserInterface } from '@/modules/v1/users/interfaces/user.interface';

@Table({
  tableName: 'users',
  underscored: true,
  paranoid: true,
})
export default class User extends Model<User> implements UserInterface {
  @Column({
    allowNull: false,
  })
  name: string;

  @Column({
    allowNull: false,
  })
  email: string;

  @Column({
    allowNull: false,
  })
  password: string;

  @Column({
    allowNull: false,
  })
  salt: string;

  @Column({
    allowNull: true,
  })
  cellphone: string;

  @Column({
    allowNull: true,
  })
  cpf: string;

  @Column({
    type: DataType.ENUM('federal', 'estadual', 'municipal', 'particular'),
    allowNull: false,
  })
  gender: GenderEnum;

  @Column({
    allowNull: false,
  })
  professionId: number;

  @Column({
    allowNull: true,
  })
  lastAccess: Date;

  @Column({
    field: 'created_at',
  })
  @CreatedAt
  createdAt: Date;

  @Column({
    field: 'updated_at',
  })
  @UpdatedAt
  updatedAt: Date;

  @Column({
    field: 'deleted_at',
  })
  @DeletedAt
  deletedAt: Date;

}
