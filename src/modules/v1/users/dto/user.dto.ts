import { IsNotEmpty, IsNumber, IsString, IsOptional, IsEnum, IsDate } from 'class-validator';
import { GenderEnum, UserInterface } from '@/modules/v1/users/interfaces/user.interface';
import { ApiProperty } from '@nestjs/swagger';

export default class UserDto implements UserInterface {
  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  readonly id?: number;

  @ApiProperty({ example: 'Ian' })
  @IsNotEmpty({ message: 'name:Required field.' })
  @IsString()
  readonly name: string;

  @ApiProperty({ example: 'ian.costa@app.com.br' })
  @IsNotEmpty({ message: 'email:Required field.' })
  @IsString()
  readonly email: string;

  @ApiProperty({ example: 'hash' })
  @IsString()
  password: string;

  @ApiProperty({ example: 'hash' })
  @IsString()
  salt: string;

  @ApiProperty({ example: '+55(71)988776655' })
  @IsOptional()
  @IsString()
  readonly cellphone?: string;

  @ApiProperty({ example: '277.996.360-33' })
  @IsOptional()
  @IsString()
  readonly cpf?: string;

  @ApiProperty({ example: 'm' })
  @IsNotEmpty({ message: 'gender:Required field.' })
  @IsEnum(GenderEnum, {
    message: 'gender must be one of: m, f or o',
  })
  readonly gender: GenderEnum;

  @ApiProperty({ example: 1 })
  @IsNotEmpty({ message: 'professionId:Required field.' })
  @IsNumber()
  readonly professionId: number;

  @ApiProperty({ example: '2022/05/03' })
  @IsDate()
  readonly lastAccess: Date;

  @ApiProperty({ example: '2022/05/03' })
  @IsOptional()
  @IsString()
  accessToken: string;
}
