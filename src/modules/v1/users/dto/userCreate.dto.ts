import { IsNotEmpty, IsString, IsNumber, IsEnum, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { GenderEnum } from '../interfaces/user.interface';

export class UserCreateDto {
  @ApiProperty({ example: 'Ian' })
  @IsNotEmpty({ message: 'name:Campo obrigatório.' })
  @IsString()
  name: string;

  @ApiProperty({ example: 'ian.costa@app.com.br' })
  @IsNotEmpty({ message: 'email:Campo obrigatório.' })
  @IsString()
  email: string;

  @ApiProperty({ example: '123456' })
  @IsNotEmpty({ message: 'password:Campo obrigatório.' })
  @IsString()
  password: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  salt?: string;

  @ApiProperty({ example: '+55(71)988776655' })
  @IsOptional()
  @IsString()
  cellphone: string;

  @ApiProperty({ example: '277.996.360-33' })
  @IsOptional()
  @IsString()
  cpf: string;

  @ApiProperty({ example: 'm' })
  @IsNotEmpty({ message: 'gender:Campo obrigatório.' })
  @IsEnum(GenderEnum, {
    message: 'gender must be one of: m, f or o',
  })
  gender: GenderEnum;

  @ApiProperty({ example: 1 })
  @IsNotEmpty({ message: 'professionId:Campo obrigatório.' })
  @IsNumber()
  professionId: number;
}
