export interface UserInterface {
  id?: number;
  name: string;
  email: string;
  password: string;
  salt?: string;
  cellphone?: string;
  cpf?: string;
  gender: GenderEnum;
  lastAccess: Date;
  professionId: number;
  accessToken?: string;
}

export enum GenderEnum {
  m = 'm',
  f = 'f',
  o = 'o',
}