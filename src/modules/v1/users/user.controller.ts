import { Body, Controller, Get, HttpCode, Post, Put, Request, UseGuards} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags, ApiBody } from '@nestjs/swagger';

import { HttpResponse, HttpResponseToFront } from '@/utils/http-response';
import { LocalAuthGuard } from '../auth/guards/local-auth.guard';
import { UserService } from '@/modules/v1/users/user.service';
import { UserCreateDto } from '@/modules/v1/users/dto/userCreate.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserUpdateDto } from './dto/userUpdate.dto';

@ApiTags('v1/users')
@Controller('v1/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Get User By Token' })
  @ApiResponse({ status: 200, type: HttpResponseToFront })
  @UseGuards(JwtAuthGuard)
  @Get()
  async showByToken(@Request() req): Promise<any> {
    const user = await this.userService.show(req.user.id);

    return HttpResponse.foundSuccessfully(user).transformToReponse();
  }

  @ApiOperation({ summary: 'Create User' })
  @ApiResponse({ status: 201, type: HttpResponseToFront })
  @ApiBody({ type: UserCreateDto })
  @Post('/')
  @HttpCode(201)
  async store(
    @Body() userCreateDto: UserCreateDto,
  ): Promise<HttpResponseToFront> {
    const user = await this.userService.store(userCreateDto);

    return HttpResponse.successfullyCreated(user).transformToReponse();
  }

  @ApiOperation({ summary: 'Update User' })
  @ApiResponse({ status: 200, type: HttpResponseToFront })
  @ApiBody({ type: UserUpdateDto })
  @UseGuards(JwtAuthGuard)
  @Put('/')
  @HttpCode(200)
  async update(
    @Body() userUpdateDto: UserUpdateDto,
    @Request() req,
  ): Promise<HttpResponseToFront> {
    const user = await this.userService.update(req.user.id, userUpdateDto);

    return HttpResponse.successfullyCreated(user).transformToReponse();
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req): Promise<any> {
    return this.userService.loginWithCredentials(req.user);
  }
}
