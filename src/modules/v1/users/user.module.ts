import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import User from '@/modules/v1/users/entities/user.entity';
import { UserController } from '@/modules/v1/users/user.controller';
import { UserService } from '@/modules/v1/users/user.service';

import { AuthService } from '../auth/auth.service';

import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constants';
import { JwtStrategy } from '../auth/strategies/jwt.strategy';

@Module({
  imports: [
    SequelizeModule.forFeature([User]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {expiresIn: '60s'}
    })
  ],
  controllers: [UserController],
  providers: [UserService, AuthService, JwtStrategy],
})
export class UserModule {}
