import { UserModule } from '@/modules/v1/users/user.module';
import { AuthModule } from './auth/auth.module';

export const modules = [UserModule, AuthModule];
