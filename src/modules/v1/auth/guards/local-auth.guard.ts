import { Injectable, CanActivate, ExecutionContext, Inject } from '@nestjs/common';

import { AuthService } from '../../auth/auth.service';

@Injectable()
export class LocalAuthGuard implements CanActivate {
    constructor(
        @Inject(AuthService)
        private readonly authService: AuthService,
      ) {
        
      }

    async canActivate(
        context: ExecutionContext,
    ): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const { email, password } = request.body;

        if (email && password) {
            let validUser = await this.authService.validateUser({ email, password });

            if (validUser) {
                validUser.password = undefined;
                validUser.salt = undefined;

                request.user = validUser;
                return true;
            }
        }

        return false;
    }
}