import { Inject, Injectable } from '@nestjs/common';
import UserDto from '../users/dto/user.dto';

import User from '../users/entities/user.entity';
import { UserService } from '../users/user.service';

import { CredentialInterface } from './interfaces/credential.interface';

@Injectable()
export class AuthService {
  constructor(
    @Inject(UserService)
    private readonly userService: UserService,
  ) { }

  async validateUser(credential: CredentialInterface): Promise<false | UserDto> {
    const userFound = await this.userService.showByEmail(credential.email);
    const pass = await this.userService.hashPassword(credential.password, userFound.salt);
    
    if (userFound && userFound.password === pass) {
      return userFound;
    }

    return false;
  }
}