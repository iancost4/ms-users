import * as faker from 'faker';

import UserDto from '@/modules/v1/users/dto/user.dto';
import { UserCreateDto } from '@/modules/v1/users/dto/userCreate.dto';
import { GenderEnum } from '@/modules/v1/users/interfaces/user.interface';

export const mockUser: UserDto = {
  id: faker.datatype.number(),
  name: faker.random.word(),
  email: faker.random.word(),
  password: faker.random.word(),
  salt: faker.random.word(),
  cellphone: faker.random.word(),
  cpf: faker.random.word(),
  gender: faker.random.arrayElement(Object.values(GenderEnum)),
  professionId: faker.datatype.number(),
  lastAccess: faker.datatype.datetime(),
  accessToken: faker.random.word(),
};

export const mockUserCreate: UserCreateDto = {
  name: faker.random.word(),
  email: faker.random.word(),
  password: faker.random.word(),
  cellphone: faker.random.word(),
  cpf: faker.random.word(),
  gender: faker.random.arrayElement(Object.values(GenderEnum)),
  professionId: faker.datatype.number(),
};

export const mockUserPromise: Promise<UserDto> = Promise.resolve(mockUser);
