# Projeto de Gerenciamento de Usuários

## Funcionalidades
 - Cadastrar usuário, editar e visualizar
 - Fazer autenticação de usuário

## Regras do negócio
- O usuário tem email, celular e cpf únicos


## Tecnologias
 - TypeScript 
 - NodeJs
 - Jest
 - Docker
 - Mysql

## Como instalar

Instalar as dependências
```bash
$ yarn install
```

## Criar o arquivo .env
```
cp .env.example .env
```

## Docker
Executar o docker usando o `docker-compose`
```bash
$ docker-compose up --build -d
```

## URL
```
http://localhost:3333
```
Esperado:
`Health check`
